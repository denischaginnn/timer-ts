import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./components/Header/Header";
import ProfilePage from "./components/Routes/ProfilePage/ProfilePage";
import SettingsPage from "./components/Routes/SettingsPage/SettingsPage";
import StartPage from "./components/Routes/StartPage/StartPage";
import StatsPage from "./components/Routes/StatsPage/StatsPage";
import TimerPage from "./components/Routes/TimerPage/TimerPage";
import "./index.css";

const App = () => {
  return (
    <BrowserRouter>
      <div className="app">
      <Header />
      <Routes>
        <Route path="/timer" element={<TimerPage />} />
        <Route path="/profile" element={<ProfilePage />} />
        <Route path="/statistics" element={<StatsPage />} />
        <Route path="/settings" element={<SettingsPage />} />
        <Route path="/" element={<StartPage />} />
      </Routes>
      </div>
    </BrowserRouter>
  );
};

export default App;
