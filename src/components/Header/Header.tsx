import React from 'react'
import Menu from './Menu/Menu'
import classes from './Header.module.css'

const Header = () => {
  return (
    <header className={classes.header} >
        <Menu />
        <h1>Speedcuber timer</h1>
    </header>
  )
}

export default Header