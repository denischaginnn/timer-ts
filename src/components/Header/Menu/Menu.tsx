import React from "react";
import MenuItem from "./MenuItem/MenuItem";
import classes from './Menu.module.css'
import timerLogo from '../../../img/svg/timer-icon.svg'
import profileLogo from '../../../img/svg/profile-icon.svg'
import statsLogo from '../../../img/svg/stats-icon.svg'
import settingsLogo from '../../../img/svg/settings-icon.svg'

const Menu = () => {
  return (
    <nav className={classes.menu}>
      <MenuItem value="Timer" imgSrc={timerLogo} imgAlt="Timer" toPath="timer"/>
      <MenuItem value="Profile" imgSrc={profileLogo} imgAlt="Profile" toPath="profile"/>
      <MenuItem value="Statistics" imgSrc={statsLogo} imgAlt="Statistics" toPath="statistics"/>
      <MenuItem value="Settings" imgSrc={settingsLogo} imgAlt="Settings" toPath="settings"/>
    </nav>
  );
};

export default Menu;
