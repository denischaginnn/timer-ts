import React, { FC } from "react";
import { NavLink } from "react-router-dom";
import classes from "./MenuItem.module.css";

interface MenuItemProps {
  value: string;
  imgSrc: string;
  imgAlt: string;
  toPath: string;
}

const MenuItem: FC<MenuItemProps> = ({ value, imgSrc, imgAlt, toPath }) => {
  return (
    <NavLink
      to={toPath}
      className={(navData) =>
        navData.isActive 
          ? [classes.menu__item, classes.active].join(" ")
          : classes.menu__item
      }
    >
      <img src={imgSrc} alt={imgAlt} />
      <p>{value}</p>
    </NavLink>
  );
};

export default MenuItem;
