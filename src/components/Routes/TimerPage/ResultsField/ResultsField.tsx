import React from "react";
import classes from "./ResultsField.module.css";

const ResultsField = () => {
  return (
    <div className={classes.block__results}>
      <div className={classes.container}>
        <p className={classes.header}>Results "first"</p>
        <p className={classes.results}>
          <span>13.19; </span>
          <span>11,47; </span>
          <span>12,49; </span>
          <span>12,49; </span>
          <span>12,49; </span>
        </p>
      </div>
    </div>
  );
};

export default ResultsField;
