import React from "react";
import classes from "./StatsBest.module.css";

const StatsBest = () => {
  return (
    <div className={classes.stats}>
      <div className={classes.stats__wrap}>
        <p>Best</p>
        <p>Try: 13.16</p>
        <p>Ao5: 13.00</p>
        <p>Ao12: --</p>
        <p>Ao50: --</p>
        <p>Ao100: --</p>
      </div>
    </div>
  );
};

export default StatsBest;
