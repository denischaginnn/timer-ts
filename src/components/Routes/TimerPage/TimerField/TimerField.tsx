import React from "react";
import classes from "./TimerField.module.css";

const Timer = () => {
  return (
    <div className={classes.timer__field}>
      <div className={classes.scramble}>
        F2 R2 D' F2 U' L2 B2 L2 U B' R' B2 R2 U2 R2 D B2 L2
      </div>
      <div className={classes.timer}>
        <span className={classes.seconds}>0</span>
        <span className={classes.millisec}>.00</span>
      </div>
    </div>
  );
};

export default Timer;
