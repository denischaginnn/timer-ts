import React, { FC } from "react";
import classes from "./CategoryItem.module.css";

interface CategoryItemProps {
  header: string;
}

const CategoryItem: FC<CategoryItemProps> = ({ header }) => {
  return (
    <div className={classes.category}>
      <p>{header}:&nbsp;</p>
      <select>
        <option>3x3</option>
        <option>4x4</option>
        <option>5x5</option>
        <option>6x6</option>
      </select>
    </div>
  );
};

export default CategoryItem;
