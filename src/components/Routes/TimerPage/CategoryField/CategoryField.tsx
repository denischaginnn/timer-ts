import React from "react";
import classes from "./CategoryField.module.css";
import CategoryItem from "./CategoryItem/CategoryItem";

const CategoryField = () => {
  return (
    <div className={classes.category__field}>
      <div>
        <CategoryItem header='Category' />
      </div>
      <div>
        <CategoryItem header='Session' />
      </div>
    </div>
  );
};

export default CategoryField;
