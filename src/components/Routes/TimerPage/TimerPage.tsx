import React from 'react'
import CategoryField from './CategoryField/CategoryField'
import ResultsField from './ResultsField/ResultsField'
import StatsAvg from './StatsAvg/StatsAvg'
import StatsBest from './StatsBest/StatsBest'
import Timer from './TimerField/TimerField'
import classes from './TimerPage.module.css'

const TimerPage = () => {
  return (
    <main className={classes.timer}>
        <CategoryField />
        <Timer />
        <ResultsField />
        <StatsAvg />
        <StatsBest />
    </main>
  )
}

export default TimerPage