import React from "react";
import classes from "./StatsAvg.module.css";

const StatsAvg = () => {
  return (
    <div className={classes.stats}>
      <div className={classes.stats__wrap}>
        <p>Average: 13.16</p>
        <p>Ao5: 13.00</p>
        <p>Ao12: --</p>
        <p>Ao50: --</p>
        <p>Ao100: --</p>
      </div>
    </div>
  );
};

export default StatsAvg;
